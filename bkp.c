/*
 * This file is provided under a MIT license.  When using or
 * redistributing this file, you may do so under either license.
 *
 * MIT License
 *
 * Copyright (c) 2020 Pavel Nadein
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * STM32F10x open source driver
 *
 * Contact Information:
 * Pavel Nadein <pavelnadein@gmail.com>
 */

#include "bkp.h"

static volatile uint16_t *get_dr_reg(uint8_t n)
{
	static volatile uint16_t *dr_regs[] = {
		&BKP->DR1,	&BKP->DR2,	&BKP->DR3,	&BKP->DR4,
		&BKP->DR5,	&BKP->DR6,	&BKP->DR7,	&BKP->DR8,
		&BKP->DR9,	&BKP->DR10,	&BKP->DR11,	&BKP->DR12,
		&BKP->DR13,	&BKP->DR14,	&BKP->DR15,	&BKP->DR16,
		&BKP->DR17,	&BKP->DR18,	&BKP->DR19,	&BKP->DR20,
		&BKP->DR21,	&BKP->DR22,	&BKP->DR23,	&BKP->DR24,
		&BKP->DR25,	&BKP->DR26,	&BKP->DR27,	&BKP->DR28,
		&BKP->DR29,	&BKP->DR30,	&BKP->DR31,	&BKP->DR32,
		&BKP->DR33,	&BKP->DR34,	&BKP->DR35,	&BKP->DR36,
		&BKP->DR37,	&BKP->DR38,	&BKP->DR39,	&BKP->DR40,
		&BKP->DR41,	&BKP->DR42,
	};

	RCC->APB1ENR |= RCC_APB1ENR_BKPEN | RCC_APB1ENR_PWREN;

	return n < ARRAY_SIZE(dr_regs) ? (volatile uint16_t *)dr_regs[n] : 0;
}

void bkp_write(uint8_t reg_num, uint16_t value)
{
	volatile uint16_t *reg = get_dr_reg(reg_num - 1);

	PWR->CR |= PWR_CR_DBP;

	if (reg)
		*reg = value;

	PWR->CR &= ~(uint32_t)PWR_CR_DBP;
}

uint16_t bkp_read(uint8_t reg_num)
{
	volatile uint16_t *reg = get_dr_reg(reg_num - 1);

	return reg ? *reg : 0;
}
